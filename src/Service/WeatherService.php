<?php

namespace App\Service;


use App\Entity\ApiResponse;
use App\Service\WeatherProvider\WeatherProvider;
use Doctrine\ORM\EntityManagerInterface;

class WeatherService
{

    // Cache timeout in seconds

    const API_CACHE_TIMEOUT = 1;

    /**
     * @var WeatherProvider
     */
    public $provider;
    private $entityManager;

    public function __construct(EntityManagerInterface $em) {
        $this->entityManager = $em;
    }

    /**
     * Set provider API for requests
     *
     * @param WeatherProvider $providerClass
     * @param $apiKey
     * @return $this
     */
    public function setProvider(WeatherProvider $providerClass, $apiKey) {
        $this->provider = $providerClass;
        $this->provider->setApiKey($apiKey);

        return $this;
    }

    public function setCity($city) {
        $this->provider->setCity($city);
        return $this;
    }


    /**
     * Set up response in provider though cache or from API
     *
     * @return $this
     */
    public function get() {

            // Convert query to md5 string
            $query = md5(json_encode($this->provider->getQuery()));

            // Find in DB by Query
            $responseCache = $this->entityManager
                ->getRepository(ApiResponse::class)
                ->findOneBy(['requestUri' => $query]);

            // If not found -> create new
            if ( ! $responseCache) {
                $responseCache = new ApiResponse();
            }

            $expires = new \DateTime('now');
            $expires->modify('-' . self::API_CACHE_TIMEOUT . ' seconds');

            // Update response in DB entity if expiry value exceeded
            if ( $responseCache->getUpdatedAt() < $expires) {
                $responseCache->setUpdatedAt(new \DateTime('now'));
                $this->provider->executeRequest();

                $responseCache->setResponse($this->provider->getResponse())->setRequestUri($query);
                $this->entityManager->persist($responseCache);
                $this->entityManager->flush();

                return $this;
            }

            // Set response in provider
            $this->provider->setResponse($responseCache->getResponse());

            return $this;
    }

    /**
     * @return WeatherProvider
     */
    public function getProvider() {
        return $this->provider;
    }
}