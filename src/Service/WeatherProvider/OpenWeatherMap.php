<?php

namespace App\Service\WeatherProvider;

class OpenWeatherMap extends AbstractWeatherProvider {

    const API_URL = 'http://api.openweathermap.org/data/2.5/weather';

    public function __construct() {
        parent::__construct();
        $this->query['units'] = 'metric';
    }

    public function setApiKey($key) {
        $this->query['appid'] = $key;
        return parent::setApiKey($key);
    }

    public function setCity($cityName) {
        $this->query['q'] = $cityName;
        return parent::setCity($cityName);
    }


    /**
     * @return $this
     */
    protected function mapData() {
        $data = json_decode($this->apiResponse);
        $this->temperature = $data->main->temp;
        $this->wind = $data->wind->speed;
        return $this;
    }
}