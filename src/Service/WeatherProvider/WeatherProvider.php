<?php

namespace App\Service\WeatherProvider;

interface WeatherProvider {

    public function setApiKey ($apiKey);

    public function setCity ($cityName);

    public function getResponse();

    public function getQuery();

    public function executeRequest();

    public function setResponse($response);
}