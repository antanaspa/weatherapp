<?php

namespace App\Service\WeatherProvider;

use GuzzleHttp\Client;

class AbstractWeatherProvider implements WeatherProvider {

    protected $apiKey, $requestClient, $apiResponse, $query;

    public $temperature, $wind, $city;

    public function __construct() {
        $this->requestClient = new Client();
    }

    /**
     * Set up API key
     *
     * @param string $key
     * @return $this
     */
    public function setApiKey($key) {
        $this->apiKey = $key;
        return $this;
    }

    /**
     * Set city name
     *
     * @param string $cityName
     * @return $this
     */
    public function setCity($cityName) {
        $this->city = $cityName;
        return $this;
    }

    /**
     * Set response from cache or API
     *
     * @param string$response
     */
    public function setResponse($response) {
        $this->apiResponse = $response;
        $this->mapData();
    }

    /**
     * Make request to API, map data from API to our vars
     * @return $this
     */
    public function executeRequest() {
        $response = $this->requestClient->request('GET', static::API_URL, [
            'query' => $this->query
        ])->getBody()->getContents();


        $this->setResponse($response);
        $this->mapData();
        return $this;
    }

    /**
     * return API response body contents
     *
     * @return mixed
     */
    public function getResponse() {
        return $this->apiResponse;
    }

    /**
     * Map fields from API to our variables
     */
    protected function mapData() {}

    /**
     * @return mixed
     */
    public function getQuery()
    {
        return $this->query;
    }

}