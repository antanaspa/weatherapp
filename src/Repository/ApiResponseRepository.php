<?php

namespace App\Repository;

use App\Entity\ApiResponse;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method ApiResponse|null find($id, $lockMode = null, $lockVersion = null)
 * @method ApiResponse|null findOneBy(array $criteria, array $orderBy = null)
 * @method ApiResponse[]    findAll()
 * @method ApiResponse[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ApiResponseRepository extends ServiceEntityRepository
{

    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, ApiResponse::class);
    }

}
