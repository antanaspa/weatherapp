<?php

namespace App\Controller;

use App\Service\WeatherProvider\OpenWeatherMap;
use App\Service\WeatherService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class WeatherController extends AbstractController {

    private function weatherForm() {
        $form = $this->createFormBuilder(null, ['attr' => ['class' => 'ajax-form']])
            ->setAction($this->generateUrl('getWeather'))
            ->setMethod('POST')
            ->add('apiKey', TextType::class)
            ->add('city', TextType::class,['attr'=>['data-id' =>'city']])
            ->add('submit', SubmitType::class)
            ->getForm();

            return $form;
    }

    public function index() {
        $form = $this->weatherForm();
        return $this->render('index.html.twig', ['form' => $form->createView()]);
    }

    public function weather(WeatherService $weatherService, Request $request) {
        $form = $this->weatherForm();
        $form->handleRequest($request);

        $city = $form->get('city')->getData();

        $data = $weatherService
            ->setProvider(new OpenWeatherMap(), $form->get('apiKey')->getData())
            ->setCity($form->get('city')->getData())
            ->get();

        return new JsonResponse(
            [
                'tabHead'       => ['targetDiv' => $city, 'htmlView' => $this->renderView('tabHead.html.twig', ['city'=> $city])],
                'tabContent'    => $this->renderView('tabContent.html.twig', [
                    'data' => $data, 'city' => $city
                ])
            ]
        );
    }
}