require('../css/app.scss');

require('bootstrap');

function Weather()
{
    this.tabs = [];


    this.addTab = function (data) {

        $('.tab-pane').each(function () {
            $(this).removeClass('show');
        });

        if ( ! this.tabs.includes(data.tabHead.targetDiv)) {
            $('#tabHead').append(data.tabHead.htmlView);
            this.tabs.push(data.tabHead.targetDiv);
            $('#tabContent').append(data.tabContent);
        } else {
            $('#'+data.tabHead.targetDiv).replaceWith(data.tabContent);
        }

        $('a#'+data.tabHead.targetDiv+'-tab').tab('show');
    };

    this.getTabContent = function (form) {
        $.ajax({
            dataType:"json",
            type: form.attr('method'),
            url: form.attr('action'),
            data: form.serialize(),
            success: function (response) {
                weather.addTab(response)
            }});

    }

}

var weather = new Weather();


$('body').on('submit', '.ajax-form', function (e) {
    e.preventDefault();
    weather.getTabContent($(this));
});
